package Constructor;

public class StartingClass {
    public static void main(String[] args) {
        Cars bmwCars = new Cars("BMW", "2019");
        bmwCars.printCarsName();
        Cars toyotaCars = new Cars();
        toyotaCars.printCarsName();
    }
}
