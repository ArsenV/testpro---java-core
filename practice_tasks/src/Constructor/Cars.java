package Constructor;

public class Cars {
    String carName = "Unknown";
    String carYear = "Unknown";

    public Cars(String insertCarName, String insertCarYear){
        this.carName = insertCarName;
        this.carYear = insertCarYear;
    }

    public Cars(){

    }

    public void printCarsName(){
        System.out.println("Name: "+ carName + "\n" + "Year: " + carYear);
    }


}
