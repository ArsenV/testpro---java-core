package FizzBuzz;

import java.util.Scanner;

public class FizzzBuzzz {
    //Input number and close scanner
    private int getInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert integer number from 1 to 100: ");
        int  input = scanner.nextInt();
        return input;
    }


    // FizzBuzz using loops
    private void fizzBuzzLoops(int start, int end) {
        for (int i = start; i <= end; i++) {
            if (i % 15 == 0) {
                System.out.println("FizzBuzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }

    // FizzBuzz using recursion
    private void fizzBuzzRecursion(int start, int end){
        if (start%15 ==0){
            System.out.println("FizzBuzz");
        } else if (start % 3 == 0) {
            System.out.println("Fizz");
        } else if (start % 5 == 0){
            System.out.println("Buzz");
        } else {
            System.out.println(start);
        }

        if (start != end) {
            fizzBuzzRecursion(start+1, end);
        }
    }


    public static void main(String[] args) {
        FizzzBuzzz fizzbuzz = new FizzzBuzzz();

        //Input the start of the range
        int inputStart = fizzbuzz.getInput();
        while ((inputStart <= 0) || (inputStart > 100)){
            System.out.println("Input should be integer and in the range from 1 to 100. Try again:");
            inputStart = fizzbuzz.getInput();
        }
        System.out.println("Your range will start from: " + inputStart);


        //Input the end of the range
        System.out.println("Please choose the end of the range");
        int inputEnd = fizzbuzz.getInput();
        while ((inputEnd < inputStart) || (inputEnd > 100)){
            System.out.println("Input should be greater then " + inputStart +" and less then 101. Try again:");
            inputEnd = fizzbuzz.getInput();
        }
        System.out.println("Your range will end at: " + inputEnd);


        //Show results
        System.out.println("\n    RESULTS FROM THE LOOP METHOD:   ");
        fizzbuzz.fizzBuzzLoops(inputStart, inputEnd);
        System.out.println("\n    RESULTS FROM THE RECURSION METHOD:   ");
        fizzbuzz.fizzBuzzRecursion(inputStart, inputEnd);
    }
}