package Polymorphism;

public class OurCar {
    public static void main(String[] args) {
        Vehicle vehicleObject = new Vehicle();
        Car carObject = new Car();

        vehicleObject.printMe();
        carObject.printMe();

        // Car extends Vehicle
        //Also we can do like this:
        // Vehicle vehicleObject = new Car();
        // vehicleObject.printMe();
        // result: I'm a Car


    }
}
