package SeleniumDemoTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



public class seleniumDemo {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","practice_tasks/src/SeleniumDemoTest/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://deens-master.now.sh/");
        String deensTitle = driver.getTitle();
        System.out.println(deensTitle);

        driver.quit();
    }
}
