package Inheritance;

public class TOYOTA extends carYear {
    public static void main(String[] args) {
        int year = 2019;
        String color = "Blue";
        String type = "SUV";

        carYear toyotaCarYear = new carYear();

        toyotaCarYear.printYear(year);
        toyotaCarYear.printColor(color);
        toyotaCarYear.printType(type);
    }
}