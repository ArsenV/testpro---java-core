import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        System.out.println("Hey, what's up.Type something:");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine().toLowerCase();
        String textNoSpaces = "";
        String revtextNoSpaces = "";

        // Let's remove all white spaces from the text. We won't use existing methods.
        for (int i = 0; i < text.length(); i++) {
            if ((text.charAt(i) >= 'a' && text.charAt(i) <= 'z') || (text.charAt(i) >= '0' && text.charAt(i) <= '9')) {
                textNoSpaces += text.charAt(i);
//                System.out.println(textNoSpaces);
            }
        }

        // Let's reverse our text
        for (int j = textNoSpaces.length() - 1; j >= 0; j--) {
            revtextNoSpaces += textNoSpaces.charAt(j);
        }

        //Lets check if it's a palindrome
        if (textNoSpaces.toLowerCase().equals(revtextNoSpaces)) {
            System.out.println("This string is palindrome");
        } else {
            System.out.println("This string is not palindrome");
        }
    }

}
